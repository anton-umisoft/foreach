<?php
$options = getopt('', array('name:', 'script:', 'dry-run::', 'no-ansi::'));

$style = function ($text) use ($options) {
	return isset($options['no-ansi']) ? '' : $text;
};

require_once __DIR__ . "/../../data/host/$options[name]/root/standalone.php";

if (isset($options['script'])) {
	if (is_readable($options['script'])) {
		require_once $options['script'];
	} else {
		echo "Can not read $options[script] file.\n";
	}
} else {
	echo "No script to run...\n";
}
